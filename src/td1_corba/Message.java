package td1_corba;


/**
* td1_corba/Message.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from forum.idl
* mardi 29 novembre 2016 15 h 05 CET
*/

public final class Message implements org.omg.CORBA.portable.IDLEntity
{
  public String title = null;
  public String author = null;
  public String date = null;
  public String body = null;

  public Message ()
  {
  } // ctor

  public Message (String _title, String _author, String _date, String _body)
  {
    title = _title;
    author = _author;
    date = _date;
    body = _body;
  } // ctor

} // class Message
