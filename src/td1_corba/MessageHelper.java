package td1_corba;


/**
* td1_corba/MessageHelper.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from forum.idl
* mardi 29 novembre 2016 15 h 05 CET
*/

abstract public class MessageHelper
{
  private static String  _id = "IDL:td1_corba/Message:1.0";

  public static void insert (org.omg.CORBA.Any a, td1_corba.Message that)
  {
    org.omg.CORBA.portable.OutputStream out = a.create_output_stream ();
    a.type (type ());
    write (out, that);
    a.read_value (out.create_input_stream (), type ());
  }

  public static td1_corba.Message extract (org.omg.CORBA.Any a)
  {
    return read (a.create_input_stream ());
  }

  private static org.omg.CORBA.TypeCode __typeCode = null;
  private static boolean __active = false;
  synchronized public static org.omg.CORBA.TypeCode type ()
  {
    if (__typeCode == null)
    {
      synchronized (org.omg.CORBA.TypeCode.class)
      {
        if (__typeCode == null)
        {
          if (__active)
          {
            return org.omg.CORBA.ORB.init().create_recursive_tc ( _id );
          }
          __active = true;
          org.omg.CORBA.StructMember[] _members0 = new org.omg.CORBA.StructMember [4];
          org.omg.CORBA.TypeCode _tcOf_members0 = null;
          _tcOf_members0 = org.omg.CORBA.ORB.init ().create_string_tc (0);
          _members0[0] = new org.omg.CORBA.StructMember (
            "title",
            _tcOf_members0,
            null);
          _tcOf_members0 = org.omg.CORBA.ORB.init ().create_string_tc (0);
          _members0[1] = new org.omg.CORBA.StructMember (
            "author",
            _tcOf_members0,
            null);
          _tcOf_members0 = org.omg.CORBA.ORB.init ().create_string_tc (0);
          _members0[2] = new org.omg.CORBA.StructMember (
            "date",
            _tcOf_members0,
            null);
          _tcOf_members0 = org.omg.CORBA.ORB.init ().create_string_tc (0);
          _members0[3] = new org.omg.CORBA.StructMember (
            "body",
            _tcOf_members0,
            null);
          __typeCode = org.omg.CORBA.ORB.init ().create_struct_tc (td1_corba.MessageHelper.id (), "Message", _members0);
          __active = false;
        }
      }
    }
    return __typeCode;
  }

  public static String id ()
  {
    return _id;
  }

  public static td1_corba.Message read (org.omg.CORBA.portable.InputStream istream)
  {
    td1_corba.Message value = new td1_corba.Message ();
    value.title = istream.read_string ();
    value.author = istream.read_string ();
    value.date = istream.read_string ();
    value.body = istream.read_string ();
    return value;
  }

  public static void write (org.omg.CORBA.portable.OutputStream ostream, td1_corba.Message value)
  {
    ostream.write_string (value.title);
    ostream.write_string (value.author);
    ostream.write_string (value.date);
    ostream.write_string (value.body);
  }

}
