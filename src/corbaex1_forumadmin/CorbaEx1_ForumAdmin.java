package corbaex1_forumadmin;

import java.io.BufferedReader;
import java.io.FileReader;
import org.omg.CORBA.ORB;
import td1_corba.ForumAdmin;
import td1_corba.ForumAdminHelper;

/**
 *
 * @author Davide Andrea Guastella <davide.guastella90@gmail.com>
 */
public class CorbaEx1_ForumAdmin {

	private static ForumAdmin FORUM;
	private static final String ADMIN_REF_FNAME = "/tmp/AdminRef";

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		connect(args);
		FORUM.Moderateur("Davide Guastella");
		FORUM.Theme("Art");
	}

	private static void connect(String[] args) {
		try {
			// create and initialize the ORB
			ORB orb = ORB.init(args, null);

			String stringIOR;
			try (BufferedReader fileReader = new BufferedReader(new FileReader(ADMIN_REF_FNAME))) {
				stringIOR = fileReader.readLine();
			}

			// get the root naming context
			org.omg.CORBA.Object objRef = orb.string_to_object(stringIOR);
			FORUM = ForumAdminHelper.narrow(objRef);
		} catch (Exception e) {
			System.out.println("ERROR : " + e);
			e.printStackTrace(System.out);
		}
	}
}
